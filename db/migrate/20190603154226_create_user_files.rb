class CreateUserFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_files do |t|
      t.integer :user_id, index: true
      t.string  :name
      t.string  :content
      t.string  :content_type
      t.string  :format
      t.integer :size

      t.timestamps
    end
  end
end
