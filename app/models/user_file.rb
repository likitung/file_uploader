class UserFile < ApplicationRecord
  mount_uploader :content, UserFileUploader
  belongs_to :user

  validates_with FileFormatValidator, FileSizeValidator

  def self.group_by_type(user)
    collection = user.user_files.includes(:user)

    return {} if collection.empty?

    final_hash = {}

    allowed_formats = APP_CONFIG['allowed_file_formats']

    allowed_formats.each do |item|
      final_hash["#{item}"] = []
    end

    collection.each do |item|
      if allowed_formats.include?(item.format)
        final_hash["#{item.format}"] << item
      end
    end

    final_hash.delete_if { |k, v| v.empty? }

    final_hash
  end
end