class FileSizeValidator < ActiveModel::Validator
  def validate(record)
    if record.size > APP_CONFIG['max_file_size']
      record.errors.add(:file_size, "#{record.size} byte is too big for upload")
    end
  end
end