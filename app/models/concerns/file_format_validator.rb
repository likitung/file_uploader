class FileFormatValidator < ActiveModel::Validator
  def validate(record)
    unless APP_CONFIG['allowed_file_formats'].include?(record.format)
      record.errors.add(:file_format, "#{record.format} not allowed")
    end
  end
end