class UserFilesController < ApplicationController
  before_action :authenticate_user!
  before_action :file_collection

  def new
    @user_file = UserFile.new
  end

  def create
    @user_file = UserFile.new(user_file_params.merge!(user: current_user))
    if @user_file.save
      flash[:notice] = 'File uploaded successful'
      redirect_to new_user_file_path
    else
      render :new
    end
  end

  private

  def file_collection
    @user_files = UserFile.group_by_type(current_user)
  end

  def user_file_params
    params.require(:user_file).permit(
      :content
    )
  end
end