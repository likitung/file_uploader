class UserFileUploader < CarrierWave::Uploader::Base
  process :save_metadata_in_model

  def save_metadata_in_model
    type, format = file.content_type.split('/') if file.content_type
    model.content_type = type
    model.format = format
    model.size = file.size
    model.name = file.filename
  end

  # Choose what kind of storage to use for this uploader:
  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
end
