# File_Uploader

..

# Running

- run `rails s` in project's home directory.

# Requirements
- Ruby = 2.4.1
- PostgreSQL 

# Setup
- Create databases and setup `config/database.yml` file
- Goto project's folder in your terminal
- run `bundle install`
- setup `config/config.yml` file for basic file validation setting for the app
- run `rails db:migrate` to migrate the database
- now run `rails s` in project's home directory to run the app server

# Specs
if you want to run the specs please run `rspec .` in project's home directory.
