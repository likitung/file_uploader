FactoryBot.define do
  factory :user do
    email { FFaker::Internet.email }
    password { 'f4k3p455w0rd' }
  end
end
