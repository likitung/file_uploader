FactoryBot.define do
  factory :user_file do
    content { File.open("#{Rails.root}/spec/fixtures/files/profile-photo-4516.jpg") }

    association :user, factory: :user
  end
end
