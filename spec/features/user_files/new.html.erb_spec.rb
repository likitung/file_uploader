require 'rails_helper'

RSpec.feature "user files listing" do
  let!(:user) { create(:user) }
  let!(:user_file) { create(:user_file, user: user) }

  it "should have files listed" do
    login_as(user, :scope => :user)
    visit 'user_files/new'
    have_selector 'td', text: 'profile-photo-4516.jpg'
    expect(page.status_code).to be(200)
  end
end
