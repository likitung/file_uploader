require 'rails_helper'

RSpec.feature "user sign in page" do
  it "200 code given" do
    visit 'users/sign_in'

    expect(page.status_code).to be(200)
  end

  it "should contain title" do
    visit 'users/sign_in'

    expect page.has_content?('Sign in')
  end
end
