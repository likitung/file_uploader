require 'rails_helper'

RSpec.feature "user sign up page" do
  it "200 code given" do
    visit 'users/sign_up'
    expect(page.status_code).to be(200)
  end

  it "should contain title" do
    visit 'users/sign_up'
    expect page.has_content?('Registration')
  end
end
