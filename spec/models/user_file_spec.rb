require 'rails_helper'

RSpec.describe UserFile, type: :model do
  let!(:user) { create(:user) }
  let!(:user_files) { create_list(:user_file, 3, user: user) }
  let!(:user_file) { UserFile }
  let!(:final_result) {{"jpeg" => user_files }}

  it { should belong_to(:user) }

  describe '#group_by_type' do
    subject { user_file.group_by_type(user) }
    it do
      is_expected.to eq(final_result)
      is_expected.not_to be_empty
    end
  end
end